# Authors : Alex and Florent

# Importing libraries :
import psycopg2
import sys
import logging_generation

# Try to connect :
try:
    conn = psycopg2.connect("dbname='f.renou' user='f.renou' host='postgresql.bts-malraux72.net'")
    # The password is stored in a ".pgpass" file
    
    # Successful connection :
    logging_generation.logging.info('Successful database connection')
    
# If failure :
except:
    print("\n\t/!\ P2Gen is unable to connect to the database :(")
    
    # Problem of connection to the database :
    logging_generation.logging.critical('The database server seems unreachable')
    sys.exit()