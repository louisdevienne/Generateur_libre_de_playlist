# Authors : Alex and Florent

import logging

# Parameters of the generation of the log lines :
logging.basicConfig(filename='p2gen_log.log',level=logging.DEBUG,\
        format='%(asctime)s -- %(levelname)s -- %(message)s')