# Authors : Alex and Florent

import argument                # Enter the arguments for the CLI
import os                      # For the bash commands
import logging_generation
from connection_db import conn # Recovery of the connection

cur = conn.cursor()

# For the sql requests and for creating playlist files :
def main():
    
    var_length=int(argument.args.length)*60 # The duration with conversion to seconds
    var_pourcent_total = 0                  # For the total percentage entered by the user
    
    if argument.args.title is not None:
        var_pourcent_total += int(argument.args.title[1])
        
        var_pourcent_title=var_length*int(argument.args.title[1])/100 # 1) The percentage title of the total duration of the playlist
        
        # 2) I look what the user wants by recovering the path and the time :
        cur.execute(
                "SELECT chemin, duree from \"RadioLibre\".morceaux where titre = \'%s\' order by random();"
                % str(argument.args.title[0])
                ) # Element 0 from the list of "title"
        
        # 3) I scan my result, I add the durations and I write all the paths directly in the playlists files :
        duree_title = 0
        fichierm3u = open("playlist.m3u", "a") # In the current folder
        
        for record in cur:
            
            # I check at the same time whether the total of the current duration does not exceed the demand :
            if duree_title < var_pourcent_title:
                duree_title += int(record[1])
                
                # I write in the m3u file created previously :
                fichierm3u.write("\n" + str(record[0]))
            
            # Else, I stop :
            else:
                fichierm3u.close() # I close the M3U file
                break              # Out of the loop "FOR"
    
    # Same thing for the rest ...
    if argument.args.album is not None:
        var_pourcent_total += int(argument.args.album[1])
        
        var_pourcent_album=var_length*int(argument.args.album[1])/100
        
        cur.execute(
                "SELECT chemin, duree from \"RadioLibre\".morceaux where album = \'%s\' order by random();"
                % str(argument.args.album[0])
                )

        duree_album = 0
        fichierm3u = open("playlist.m3u", "a")
        
        for record in cur:
            
            if duree_album < var_pourcent_album:
                duree_album += int(record[1])
                
                fichierm3u.write("\n" + str(record[0]))
            
            else:
                fichierm3u.close()
                break
    
    if argument.args.artist is not None:
        var_pourcent_total += int(argument.args.artist[1])
        
        var_pourcent_artist=var_length*int(argument.args.artist[1])/100        
        
        cur.execute(
                "SELECT chemin, duree from \"RadioLibre\".morceaux where artiste = \'%s\' order by random();"
                % str(argument.args.artist[0])
                )
        
        duree_artist = 0
        fichierm3u = open("playlist.m3u", "a")
        
        for record in cur:
            
            if duree_artist < var_pourcent_artist:
                duree_artist += int(record[1])
                
                fichierm3u.write("\n" + str(record[0]))
            
            else:
                fichierm3u.close()
                break
    
    if argument.args.genre is not None:
        var_pourcent_total += int(argument.args.genre[1])
        
        var_pourcent_genre=var_length*int(argument.args.genre[1])/100 
        
        cur.execute(
                "SELECT chemin, duree from \"RadioLibre\".morceaux where genre = \'%s\' order by random();"
                % str(argument.args.genre[0])
                )
        
        duree_genre = 0
        fichierm3u = open("playlist.m3u", "a")
        
        for record in cur:
            
            if duree_genre < var_pourcent_genre:
                duree_genre += int(record[1])
                
                fichierm3u.write("\n" + str(record[0]))
            
            else:
                fichierm3u.close()
                break
    
    if argument.args.subgenre is not None:
        var_pourcent_total += int(argument.args.subgenre[1])
        
        var_pourcent_subgenre=var_length*int(argument.args.subgenre[1])/100         
        
        cur.execute(
                "SELECT chemin, duree from \"RadioLibre\".morceaux where sousgenre = \'%s\' order by random();"
                % str(argument.args.subgenre[0])
                )
        
        duree_subgenre = 0
        for record in cur:
            
            if duree_subgenre < var_pourcent_subgenre:
                duree_subgenre += int(record[1])
                
                fichierm3u.write("\n" + str(record[0]))
            
            else:
                fichierm3u.close()
                break
    
    # Check if the total percentage is exceeded :
    if var_pourcent_total > 100:
        exception("Incorrect percentage entry","The percentage accumulation of arguments exceeds 100%")
            
def exception(why,log):
    
    # Error message and log :
    print("\n\t/!\ " + why)
    logging_generation.logging.critical(log)
    
    # Before, we check if the M3U playlist file exists :
    try:
        with open('playlist.m3u'):
            os.remove('playlist.m3u') # And we delete it to recreate it after
    # Else, we pass because we have nothing to do :
    except: pass