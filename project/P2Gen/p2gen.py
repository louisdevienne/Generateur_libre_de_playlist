# Authors : Alex and Florent

import sys
sys.path.insert(0, "src/") # Adds the "src" folder to the Python path for the modules

import connection_db # For the connection to the database
import os            # For the bash commands
import functions     # Contains functions

# If the user enter a value for a argument with function in "functions.py" :

# Before, we check if the M3U playlist file exists :
try:
    with open('playlist.m3u'):
        os.remove('playlist.m3u') # And we delete it to recreate it after
# Else, we pass because we have nothing to do :
except: pass

functions.main()