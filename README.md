# Projet Générateur de playlist

https://framagit.org/sio/ppe3/ppe3-generateur-de-playlist/blob/master/GenerateurDePlaylist.pdf

Développement en **Python 3** d'un générateur de playlist libre appelé `P2Gen` _Python Playlist Generator_ exploitable par `Cmus`, aux formats `M3U` et `XSPF` pour la station radio du lycée André Malraux.

Auteurs : **RENOU Florent** et **RENOU Alex** BTS SIO 2ème année