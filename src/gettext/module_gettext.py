import gettext # For i18n

try:
    traduction = gettext.translation('translate_file', localedir='local', language=['fr']) # Load the translation file "translate_file.po" of the specified language (here "fr", so French), and look for it in the folder "local".
    traduction.install('translate_file') # For interpretation

except:
    gettext.install('my text') # If the file does not exist, we will work on the hard-coded text.