'''Authors : RENOU Florent
             RENOU Alex
'''

import argparse as AP        # Importing the argparse module with a alias
parser = AP.ArgumentParser() # Use of the function

parser.add_argument('length', action='store', help='The total duration of the playlist in minutes', metavar=('MINUTE')) # Positional argument

# Optional arguments :
group = parser.add_argument_group('group', 'settings')

# Add to group :
group.add_argument('-t', '--title',    action='store', nargs=2, help='The amount of a title to put in the playlist',   metavar=('TITLE',   'POURCENT')) # nargs = list[]
group.add_argument('-A', '--album',    action='store', nargs=2, help='The amount of album to put in the playlist',     metavar=('ALBUM',   'POURCENT'))
group.add_argument('-a', '--artist',   action='store', nargs=2, help='The amount of artist to put in the playlist',    metavar=('ARTIST',  'POURCENT'))
group.add_argument('-g', '--genre',    action='store', nargs=2, help='The amount of genre to put in the playlist',     metavar=('GENRE',   'POURCENT'))
group.add_argument('-s', '--subgenre', action='store', nargs=2, help='The amount of sub genre to put in the playlist', metavar=('SUBGENRE','POURCENT'))

args = parser.parse_args()