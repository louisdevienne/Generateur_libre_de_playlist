# Example of execution for logs generation
import logging
import os # For Linux commands

# The choice of execution :
choice = str(input('\nBasic Test (1), Separation of log files (2), Rotation of log files (3) or Rotation of log files with a time (4) : '))

if choice == '1':
    logging.basicConfig(filename='test_log.log',level=logging.DEBUG,\
        format='%(asctime)s -- %(name)s -- %(levelname)s -- %(message)s') # Parameter of the generation of the log lines
    logging.debug('Debug error') # We notice the errors in the log file with the previous settings
    logging.info('INFO ERROR')
    logging.warning('Warning Error %s: %s', '01234', 'Generation Error')
    logging.error('error message')
    logging.critical('critical error')
    
    # Read file with a Linux command 'cat' :
    os.system('cat test_log.log')

elif choice == '2':
    formatter = logging.Formatter("%(asctime)s -- %(name)s -- %(levelname)s -- %(message)s") # Sets the desired format for messages
    
    handler_critic = logging.FileHandler("critic.log", mode="a", encoding="utf-8") # Creation of 2 handlers, one by file, for the log critic, and one for the log of type INFO.
    handler_info = logging.FileHandler("info.log", mode="a", encoding="utf-8")     # We give them the name of the file, the writing mode and the type of encoding to use
    
    handler_critic.setFormatter(formatter) # Format of messages to use (the var "formatter" previously used)
    handler_info.setFormatter(formatter)
    
    handler_info.setLevel(logging.INFO) # Level of sensitivity of the logs :
    handler_critic.setLevel(logging.CRITICAL)
    
    logger = logging.getLogger("nom_programme") # Then we create a "logger" object by assigning it a name, a minimal level of sensitivity,
    logger.setLevel(logging.INFO)               # and linking it to the two previously created handlers
    logger.addHandler(handler_critic)
    logger.addHandler(handler_info)
    
    logger.debug('Debug error') # Transmission of log messages
    logger.info('INFO ERROR')
    logger.critical('INFO ERROR2')
    # /!\ AS A REMINDER, THE HANDLERS CORRESPOND TO THE OUTPUT OF THE LOGS, AND THE LOGGER TO THE INPUT, PROGRAM SIDE !

elif choice == '3':
    from logging.handlers import RotatingFileHandler # Import the new handlers
    
    formatter_debug = logging.Formatter("%(asctime)s -- %(name)s -- %(levelname)s -- %(message)s")
    formatter_info = logging.Formatter("%(asctime)s -- %(name)s -- %(levelname)s -- %(message)s")
    
    logger_debug = logging.getLogger("debug_log") # Specifies the new handlers :
    handler_debug = RotatingFileHandler("debug.log", mode="a", maxBytes= 100, backupCount= 1 , encoding="utf-8")
    handler_debug.setFormatter(formatter_debug)
    logger_debug.setLevel(logging.DEBUG)
    logger_debug.addHandler(handler_debug)
    
    logger_info = logging.getLogger("info_log")
    handler_info = logging.handlers.RotatingFileHandler("info.log", mode="a", maxBytes= 10, backupCount= 1,encoding="utf-8")
    handler_info.setFormatter(formatter_info)
    logger_info.setLevel(logging.INFO)
    logger_info.addHandler(handler_info)
    
    logger_debug.debug('Debug error')
    logger_info.debug('DEBUG ERROR')
    logger_info.info('INFO ERROR')
    logger_info.critical('INFO ERROR2')

elif choice == '4':
    from logging.handlers import TimedRotatingFileHandler
    import time
    
    formatter_debug = logging.Formatter("%(asctime)s -- %(name)s -- %(levelname)s -- %(message)s")
    formatter_info = logging.Formatter("%(asctime)s -- %(name)s -- %(levelname)s -- %(message)s")
    
    logger_debug = logging.getLogger("debug_log") # A new log is created every 5 seconds
    handler_debug = TimedRotatingFileHandler("debug.log", when="s", interval=5, encoding="utf-8")
    handler_debug.setFormatter(formatter_debug)
    logger_debug.setLevel(logging.DEBUG)
    logger_debug.addHandler(handler_debug)
    
    logger_info = logging.getLogger("info_log")
    handler_info = logging.handlers.TimedRotatingFileHandler("info.log", when="s", interval=5, encoding="utf-8")
    handler_info.setFormatter(formatter_info)
    logger_info.setLevel(logging.INFO)
    logger_info.addHandler(handler_info)
    
    logger_debug.debug('Debug error')
    logger_info.debug('DEBUG ERROR')
    logger_info.info('INFO ERROR')
    time.sleep(7)
    logger_info.critical('INFO ERROR2')

else:
    print("\n/!\ Input Error !")
