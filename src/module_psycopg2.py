# Importing libraries :
import psycopg2

# Try to connect :
try:
    conn = psycopg2.connect("dbname='a.renou' user='a.renou' host='postgresql.bts-malraux72.net'")
    # The password is stored in a ".pgpass" file
    
    print ("\nThe connection is established :)")
    
# If failure :
except:
    print("I am unable to connect to the database :(")
    
cur = conn.cursor()
try:
    # Selection of table :
    cur.execute("SET schema 'RadioLibre'; SELECT * from morceaux FETCH FIRST 15 ROWS ONLY;")
    print ("I select the data base :)")

except:
    print ("I can't select the data base :(")
    
# View data ? (with 15 lines)
choice = str(input("\nDo you want to display the data of the table ? (o/n) "))

# Choice management :

# If yes :
if choice == "o":
    # Displaying table data : (the variable "display" displays all the columns of the database)
    # Names of the columns with organization :
    print("\n")
    print ("|" + "\t" + "TITLE"     + "\t" +
           "|" + "\t" + "ALBUM"     + "\t" +
           "|" + "\t" + "ARTIST"    + "\t" +
           "|" + "\t" + "KIND"      + "\t" +
           "|" + "\t" + "SUB GENRE" + "\t" +
           "|" + "\t" + "LENGTH"    + "\t" +
           "|" + "\t" + "FORMAT"    + "\t" +
           "|" + "\t" + "POLYPHONY" + "\t" +
           "|" + "\t" + "PATH"      + "\t" + "|" + "\n")
    
    # Browse the data of the table "morceaux" :
    for display in cur.fetchall(): # Conversion to string the 9 values of the table
        print ("|" + "\t" + str(display[0]) + "\t" +
               "|" + "\t" + str(display[1]) + "\t" +
               "|" + "\t" + str(display[2]) + "\t" +
               "|" + "\t" + str(display[3]) + "\t" +
               "|" + "\t" + str(display[4]) + "\t" +
               "|" + "\t" + str(display[5]) + "\t" +
               "|" + "\t" + str(display[6]) + "\t" +
               "|" + "\t" + str(display[7]) + "\t" +
               "|" + "\t" + str(display[8]) + "\t" + "|" + "\n")

# Ester egg french :
elif choice == "abricot" or choice == "ABRICOT":
    print("/!\ Bah non :(")
    
# If error of choice :
elif choice != "o" and choice != "n":
    print("/!\ Input error !")